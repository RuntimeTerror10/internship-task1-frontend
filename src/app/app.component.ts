import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  menuOpen = false;

  menuToggle(isOpen: boolean) {
    this.menuOpen = isOpen;
  }

  toggleCloseMenu() {
    this.menuOpen = false;
  }
  title = 'Internship-task-Frontend-1';
}
