import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  @Output() toggleSideBar = new EventEmitter();
  @Input() isOpen: boolean;

  toggleMenu() {
    this.isOpen = !this.isOpen;
    this.toggleSideBar.emit(this.isOpen);
  }

  constructor() {}

  ngOnInit(): void {}
}
