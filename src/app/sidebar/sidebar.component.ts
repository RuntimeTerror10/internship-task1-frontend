import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
})
export class SidebarComponent implements OnInit {
  @Input() toggleMenuOpen: boolean;
  @Output() toggleMenuClose = new EventEmitter();

  closeMenu() {
    this.toggleMenuOpen = false;
    this.toggleMenuClose.emit();
  }

  constructor() {}

  ngOnInit(): void {}
}
